# Machine Learning and Deep Learning in Python  

## Numpy Basics  

1. Examples of Data Loading, Classification, Linear Regression, Random Forests, Deep Learning, etc.

## Linear Regression  

1. 1D Linear Regression  
2. Multilinear Regression & Polynomial Regression  
3. Practical Issues  

## Logistic Regression  

1. 