# N.B. May need to update the version of future: sudo pip install -U 

# Imports
import numpy as np
import matplotlib.pyplot as plt
from __future__ import print_function, division
from builtins import range
from mpl_toolkits.mplot3d import Axes3D

# Load the Data.
X = []
Y = []
for line in open('data_2d.csv'):
    x1, x2, y = line.split(',')
    # Add the bias term, x0 = 1, at the end.
    X.append([float(x1), float(x2), 1]) 
    Y.append(float(y))

# Turn X and Y into numpy arrays since that will be useful later.
X = np.array(X)
Y = np.array(Y)

# Plot the data to see what it looks like.
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X[:,0], X[:,1], Y)
plt.show()

# Calculate the weights parameters a and b.
# N.B. numpy has a special method for solving Ax = b, so we don't use x = inv(A) * b
# N.B. The * operator does element-by-element multiplication in numpy.
# N.B. * does element-by-element multiplication. We want matrix multiplication. 
# np.dot() usually does what we expect for matrix multiplication.
w = np.linalg.solve(np.dot(X.T, X), np.dot(X.T, Y))
Yhat = np.dot(X, w)

# Determine how good the model is by computing the R-squared.
d1 = Y - Yhat
d2 = Y - Y.mean()
r2 = 1 - d1.dot(d1) / d2.dot(d2)
print("the r-squared is:", r2)