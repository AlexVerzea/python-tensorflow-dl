# N.B. May need to update the version of future: sudo pip install -U 

# Imports
import numpy as np
import matplotlib.pyplot as plt
from __future__ import print_function, division
from builtins import range

# Load the Data.
X = []
Y = []
for line in open('data_poly.csv'):
    x, y = line.split(',')
    # x is just a scalar.
    x = float(x)
    # Add the bias term x0 = 1.
    X.append([1, x, x*x])
    # The model is therefore y_hat = w0 + w1 * x + w2 * x^2
    Y.append(float(y))

# Turn X and Y into numpy arrays since that will be useful later.
X = np.array(X)
Y = np.array(Y)

# Plot the data to see what it looks like.
plt.scatter(X[:,1], Y)
plt.title("The data we're trying to fit")
plt.show()


# Calculate the weights/parameters a and b.
# N.B. numpy has a special method for solving Ax = b, so we don't use x = inv(A) * b
# N.B. The * operator does element-by-element multiplication in numpy.
# N.B. * does element-by-element multiplication. We want matrix multiplication. 
# np.dot() usually does what we expect for matrix multiplication.
w = np.linalg.solve(np.dot(X.T, X), np.dot(X.T, Y))

# Plot everything together to make sure it worked.
plt.scatter(X[:,1], Y)

# To plot the quadratic model predictions, create a line of x's and calculate the predicted y's.
x_line = np.linspace(X[:,1].min(), X[:,1].max())
y_line = w[0] + w[1] * x_line + w[2] * x_line * x_line
plt.plot(x_line, y_line)
plt.title("Our fitted quadratic")
plt.show()

# Determine how good the model is by computing the R-squared.
Yhat = X.dot(w)
d1 = Y - Yhat
d2 = Y - Y.mean()
r2 = 1 - d1.dot(d1) / d2.dot(d2)
print("the r-squared is:", r2)