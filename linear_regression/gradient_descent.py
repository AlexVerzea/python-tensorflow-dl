# N.B. May need to update the version of future: sudo pip install -U 

# Imports
import numpy as np
import matplotlib.pyplot as plt
from __future__ import print_function, division
from builtins import range

N = 10
D = 3
X = np.zeros((N, D))
X[:,0] = 1 # bias term
X[:5,1] = 1
X[5:,2] = 1
Y = np.array([0]*5 + [1]*5)

# Print X so we know what it looks like.
print("X:", X)

# This won't work because the matrix is singular (not invertible, columns not independent): 
# w = np.linalg.solve(X.T.dot(X), X.T.dot(Y))

# Instead, we use gradient descent.
# Keep track of squared error cost.
costs = [] 
# Randomly initialize w.
w = np.random.randn(D) / np.sqrt(D)
learning_rate = 0.001
for t in range(1000):
  # Update w.
  Yhat = X.dot(w)
  delta = Yhat - Y
  w = w - learning_rate*X.T.dot(delta)
  # Find and store the cost.
  mse = delta.dot(delta) / N
  costs.append(mse)

# Plot the costs.
plt.plot(costs)
plt.show()

print("final w:", w)

# Plot prediction vs target.
plt.plot(Yhat, label='prediction')
plt.plot(Y, label='target')
plt.legend()
plt.show()