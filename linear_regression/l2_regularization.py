# N.B. May need to update the version of future: sudo pip install -U 

# Imports
import numpy as np
import matplotlib.pyplot as plt
from __future__ import print_function, division
from builtins import range

N = 50

# Generate the Data.
X = np.linspace(0,10,N)
Y = 0.5 * X + np.random.randn(N)

# Generate outliers.
Y[-1] += 30
Y[-2] += 30

# Plot the Data.
plt.scatter(X, Y)
plt.show()

# Add bias term.
X = np.vstack([np.ones(N), X]).T

# Plot the maximum likelihood solution.
w_ml = np.linalg.solve(X.T.dot(X), X.T.dot(Y))
Yhat_ml = X.dot(w_ml)
plt.scatter(X[:,1], Y)
plt.plot(X[:,1], Yhat_ml)
plt.show()

# Plot the regularized solution.
# N.B. We probably don't need an L2 regularization this high in many problems
l2 = 1000.0
w_map = np.linalg.solve(l2*np.eye(2) + X.T.dot(X), X.T.dot(Y))
Yhat_map = X.dot(w_map)
plt.scatter(X[:,1], Y)
plt.plot(X[:,1], Yhat_ml, label='maximum likelihood')
plt.plot(X[:,1], Yhat_map, label='map')
plt.legend()
plt.show()