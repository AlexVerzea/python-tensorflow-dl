# Transistor count from: https://en.wikipedia.org/wiki/Transistor_count
# N.B. May need to update the version of future: sudo pip install -U 

# Imports.
import re
import numpy as np
import matplotlib.pyplot as plt
from __future__ import print_function, division
from builtins import range

X = []
Y = []

# Some numbers show up as 1,170,000,000 (commas) and ome numbers have references in 
# square brackets after them. This removed Wikipedia references.
non_decimal = re.compile(r'[^\d]+')

for line in open('moore.csv'):
    r = line.split('\t')
    x = int(non_decimal.sub('', r[2].split('[')[0]))
    y = int(non_decimal.sub('', r[1].split('[')[0]))
    X.append(x)
    Y.append(y)

X = np.array(X)
Y = np.array(Y)

plt.scatter(X, Y)
plt.show()

Y = np.log(Y)
plt.scatter(X, Y)
plt.show()

# Copy from lr_1d.py.
denominator = X.dot(X) - X.mean() * X.sum()
a = ( X.dot(Y) - Y.mean()*X.sum() ) / denominator
b = ( Y.mean() * X.dot(X) - X.mean() * X.dot(Y) ) / denominator

# Calculate the predicted Y.
Yhat = a * X + b

plt.scatter(X, Y)
plt.plot(X, Yhat)
plt.show()

# Determine how good the model is by computing the R-squared.
d1 = Y - Yhat
d2 = Y - Y.mean()
r2 = 1 - d1.dot(d1) / d2.dot(d2)
print("a:", a, "b:", b)
print("the r-squared is:", r2)

# How long does it take to double? year1 and year2 refer to different years, 
# though not necessarily consecutive.
# log(transistorcount) = a * year + b
# transistorcount = exp(b) * exp(a * year)
# 2 * transistorcount = 2 * exp(b) * exp(a * year) = exp(ln(2)) * exp(b) * exp(a * year) = exp(b) * exp(a * year + ln(2))
# a * year2 = a * year1 + ln2
# year2 = year1 + ln2/a
print("time to double:", np.log(2)/a, "years")
