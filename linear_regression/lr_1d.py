# N.B. May need to update the version of future: sudo pip install -U 

# Imports
import numpy as np
import matplotlib.pyplot as plt
from __future__ import print_function, division
from builtins import range

# Load the data. Convert x to float and append to X. Same for y.
X = []
Y = []
for line in open('./data_1d.csv'):
    x, y = line.split(',')
    X.append(float(x))
    Y.append(float(y))

# Turn X and Y into numpy arrays, which will be useful later.
X = np.array(X)
Y = np.array(Y)

# Plot the data to see what it looks like.
plt.scatter(X, Y)
plt.show()


# Calculate the slope and intercept. Remember that the denominator is common.
# and can be calculated separately.
# N.B. This could be more efficient if we only computed the sums and means once.
denominator = X.dot(X) - X.mean() * X.sum()
a = ( X.dot(Y) - Y.mean()*X.sum() ) / denominator
b = ( Y.mean() * X.dot(X) - X.mean() * X.dot(Y) ) / denominator

# Calculate the predicted Y.
Yhat = a * X + b

# Plot everything together to make sure it works.
plt.scatter(X, Y)
plt.plot(X, Yhat)
plt.show()

# Determine how good the model is by computing the R-squared.
d1 = Y - Yhat
# Y is a vector and Y.mean() is a scalar but Python understands what we're
# trying to do and subtracts the scalar from every vector element.
d2 = Y - Y.mean() 
r2 = 1 - d1.dot(d1) / d2.dot(d2)
print("the r-squared is:", r2)
