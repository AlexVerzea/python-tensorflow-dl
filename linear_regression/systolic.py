# N.B. Need to sudo pip install xlrd to use pd.read_excel
# N.B. May need to update the version of future: sudo pip install -U 
# N.B. Data is from: http://college.cengage.com/mathematics/brase/understandable_statistics/7e/students/datasets/mlr/frames/mlr02.html

# The data (X1, X2, X3) are for each patient.
# X1 = systolic blood pressure
# X2 = age in years
# X3 = weight in pounds

# Imports
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from __future__ import print_function, division
from builtins import range

df = pd.read_excel('mlr02.xls')
X = df.values

# Using the age to predict the systolic blood pressure.
plt.scatter(X[:,1], X[:,0])
plt.show()

# Using the weight to predict the systolic blood pressure
plt.scatter(X[:,2], X[:,0])
plt.show()

# This will act as the Bias.
df['ones'] = 1
Y = df['X1']
X = df[['X2', 'X3', 'ones']]
X2only = df[['X2', 'ones']]
X3only = df[['X3', 'ones']]

# Determine how good the model is by computing the R-squared.
def get_r2(X, Y):
    w = np.linalg.solve( X.T.dot(X), X.T.dot(Y) )
    Yhat = X.dot(w)
    d1 = Y - Yhat
    d2 = Y - Y.mean()
    r2 = 1 - d1.dot(d1) / d2.dot(d2)
    return r2

print("r2 for x2 only:", get_r2(X2only, Y))
print("r2 for x3 only:", get_r2(X3only, Y))
# Using both gives a better R-squared.
print("r2 for both:", get_r2(X, Y))