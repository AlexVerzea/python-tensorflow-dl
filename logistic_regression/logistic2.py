# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import numpy as np
from builtins import range

N = 100
D = 2

X = np.random.randn(N,D)

# Center the first 50 points at (-2,-2).
X[:50,:] = X[:50,:] - 2 * np.ones((50,D))

# Center the last 50 points at (2, 2).
X[50:,:] = X[50:,:] + 2 * np.ones((50,D))

# Labels: first 50 are 0, last 50 are 1.
T = np.array([0] * 50 + [1] * 50)

# Add a column of ones.
# ones = np.array([[1]*N]).T # old
ones = np.ones((N, 1))
Xb = np.concatenate((ones, X), axis=1)

# Randomly initialize the weights.
w = np.random.randn(D + 1)

# Calculate the model output.
z = Xb.dot(w)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

Y = sigmoid(z)

# Calculate the cross-entropy error.
def cross_entropy(T, Y):
    E = 0
    for i in range(len(T)):
        if T[i] == 1:
            E -= np.log(Y[i])
        else:
            E -= np.log(1 - Y[i])
    return E

print(cross_entropy(T, Y))

# Try it with the closed-form solution.
w = np.array([0, 4, 4])

# Calculate the model output.
z = Xb.dot(w)
Y = sigmoid(z)

# Calculate the cross-entropy error.
print(cross_entropy(T, Y))