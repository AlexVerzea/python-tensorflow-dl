# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import numpy as np
import matplotlib.pyplot as plt
from builtins import range

N = 4
D = 2

# XOR Gate.
X = np.array([
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1],
])
T = np.array([0, 1, 1, 0])

# Add a column of ones.
ones = np.ones((N, 1))

# Add a column of xy = x * y.
Xb = np.concatenate((ones, X), axis=1)

# Randomly initialize the weights.
w = np.random.randn(D + 1)

# Calculate the model output.
z = Xb.dot(w)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

Y = sigmoid(z)

# Calculate the cross-entropy error.
def cross_entropy(T, Y):
    return - (T * np.log(Y) + (1-T) * np.log(1-Y)).sum()

# Let's do gradient descent 100 times.
learning_rate = 0.001
error = []
w_mags = []
for i in range(100000):
    e = cross_entropy(T, Y)
    error.append(e)
    if i % 1000 == 0:
        print(e)

    # Gradient descent weight udpate with regularization.
    w += learning_rate * Xb.T.dot(T - Y)

    w_mags.append(w.dot(w))

    # Recalculate Y
    Y = sigmoid(Xb.dot(w))

plt.plot(error)
plt.title("Cross-entropy per iteration")
plt.show()

plt.plot(w_mags)
plt.title("w^2 magnitudes")
plt.show()

print("Final w:", w)
print("Final classification rate:", 1 - np.abs(T - np.round(Y)).sum() / N)