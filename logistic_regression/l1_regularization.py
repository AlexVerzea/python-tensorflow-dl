# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
from builtins import range
import numpy as np
import matplotlib.pyplot as plt

def sigmoid(z):
  return 1/(1 + np.exp(-z))

N = 50
D = 50

# Uniformly distributed numbers between -5, +5.
X = (np.random.random((N, D)) - 0.5) * 10

# True weights - only the first 3 dimensions of X affect Y.
true_w = np.array([1, 0.5, -0.5] + [0] * (D - 3))

# Generate Y - add noise with variance 0.5.
Y = np.round(sigmoid(X.dot(true_w) + np.random.randn(N) * 0.5))

# Perform gradient descent to find w.
costs = [] # Keep track of squared error cost.
w = np.random.randn(D) / np.sqrt(D) # Randomly initialize w.
learning_rate = 0.001
l1 = 3.0 # Try different values - what effect does it have on w?
for t in range(5000):
  # Update w.
  Yhat = sigmoid(X.dot(w))
  delta = Yhat - Y
  w = w - learning_rate*(X.T.dot(delta) + l1 * np.sign(w))

  # Find and store the cost.
  cost = -(Y * np.log(Yhat) + (1-Y) * np.log(1 - Yhat)).mean() + l1*np.abs(w).mean()
  costs.append(cost)

# Plot the costs.
plt.plot(costs)
plt.show()

print("final w:", w)

# Plot our w vs true w.
plt.plot(true_w, label='true w')
plt.plot(w, label='w_map')
plt.legend()
plt.show()