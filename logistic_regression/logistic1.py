# N.B. May need to update the version of future: sudo pip install -U 

# Imports
import numpy as np
from __future__ import print_function, division
from builtins import range

N = 100
D = 2

# Data X and weight matrix w are randomly generated from a standard normal distribution.
X = np.random.randn(N,D)
w = np.random.randn(D + 1)

# ones = np.array([[1]*N]).T # old
ones = np.ones((N, 1))
Xb = np.concatenate((ones, X), axis=1)

z = Xb.dot(w)
def sigmoid(z):
    return 1/(1 + np.exp(-z))
print(sigmoid(z))