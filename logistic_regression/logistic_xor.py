# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import numpy as np
import matplotlib.pyplot as plt
from builtins import range

N = 4
D = 2

# XOR
X = np.array([
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1],
])
T = np.array([0, 1, 1, 0])

# Add a column of ones.
# ones = np.array([[1] * N]).T
ones = np.ones((N, 1))

# Add a column of xy = x * y
xy = (X[:,0] * X[:,1]).reshape(N, 1)
Xb = np.concatenate((ones, xy, X), axis=1)

# Randomly initialize the weights.
w = np.random.randn(D + 2)

# Calculate the model output.
z = Xb.dot(w)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

Y = sigmoid(z)

# Calculate the cross-entropy error.
def cross_entropy(T, Y):
    E = 0
    for i in range(len(T)):
        if T[i] == 1:
            E -= np.log(Y[i])
        else:
            E -= np.log(1 - Y[i])
    return E

# Let's do gradient descent 100 times.
learning_rate = 0.01
error = []
for i in range(10000):
    e = cross_entropy(T, Y)
    error.append(e)
    if i % 1000 == 0:
        print(e)

    # Gradient descent weight udpate with regularization.
    w += learning_rate * ( Xb.T.dot(T - Y) - 0.01*w )

    # Recalculate Y.
    Y = sigmoid(Xb.dot(w))

plt.plot(error)
plt.title("Cross-entropy per iteration")
plt.show()

print("Final w:", w)
print("Final classification rate:", 1 - np.abs(T - np.round(Y)).sum() / N)