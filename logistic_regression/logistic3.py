# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import numpy as np
import matplotlib.pyplot as plt
from builtins import range

N = 100
D = 2

N_per_class = N//2

X = np.random.randn(N,D)

# Center the first 50 points at (-2,-2).
X[:N_per_class,:] = X[:N_per_class,:] - 2*np.ones((N_per_class,D))

# Center the last 50 points at (2, 2).
X[N_per_class:,:] = X[N_per_class:,:] + 2*np.ones((N_per_class,D))

# Labels: first N_per_class are 0, last N_per_class are 1.
T = np.array([0]*N_per_class + [1]*N_per_class)

# Add a column of ones.
# ones = np.array([[1]*N]).T # old
ones = np.ones((N, 1))
Xb = np.concatenate((ones, X), axis=1)

# Randomly initialize the weights.
w = np.random.randn(D + 1)

# Calculate the model output.
z = Xb.dot(w)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

Y = sigmoid(z)

# Calculate the cross-entropy error.
def cross_entropy(T, Y):
    E = 0
    for i in range(len(T)):
        if T[i] == 1:
            E -= np.log(Y[i])
        else:
            E -= np.log(1 - Y[i])
    return E


# Let's do gradient descent 100 times.
learning_rate = 0.1
for i in range(100):
    if i % 10 == 0:
        print(cross_entropy(T, Y))

    # Gradient descent weight udpate.
    # w += learning_rate * np.dot((T - Y).T, Xb) # old
    w += learning_rate * Xb.T.dot(T - Y)

    # Recalculate Y.
    Y = sigmoid(Xb.dot(w))


print("Final w:", w)

# Plot the data and separating line.
plt.scatter(X[:,0], X[:,1], c=T, s=100, alpha=0.5)
x_axis = np.linspace(-6, 6, 100)
y_axis = -(w[0] + x_axis*w[1]) / w[2]
plt.plot(x_axis, y_axis)
plt.show()