# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import numpy as np
import matplotlib.pyplot as plt
from builtins import range

N = 1000
D = 2

R_inner = 5
R_outer = 10

# Distance from origin is radius + random normal.
# Angle theta is uniformly distributed between (0, 2pi).
R1 = np.random.randn(N//2) + R_inner
theta = 2 * np.pi*np.random.random(N//2)
X_inner = np.concatenate([[R1 * np.cos(theta)], [R1 * np.sin(theta)]]).T

R2 = np.random.randn(N//2) + R_outer
theta = 2 * np.pi*np.random.random(N//2)
X_outer = np.concatenate([[R2 * np.cos(theta)], [R2 * np.sin(theta)]]).T

X = np.concatenate([ X_inner, X_outer ])
T = np.array([0] * (N//2) + [1] * (N//2)) # Labels: first 50 are 0, last 50 are 1.

plt.scatter(X[:,0], X[:,1], c=T)
plt.show()

# Add a column of ones.
ones = np.ones((N, 1))

# Add a column of r = sqrt(x^2 + y^2).
r = np.sqrt( (X * X).sum(axis=1) ).reshape(-1, 1)
Xb = np.concatenate((ones, r, X), axis=1)

# Randomly initialize the weights.
w = np.random.randn(D + 2)

# Calculate the model output.
z = Xb.dot(w)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

Y = sigmoid(z)

# Calculate the cross-entropy error.
def cross_entropy(T, Y):
    return -(T*np.log(Y) + (1-T)*np.log(1-Y)).sum()

# Let's do gradient descent 100 times.
learning_rate = 0.0001
error = []
for i in range(5000):
    e = cross_entropy(T, Y)
    error.append(e)
    if i % 500 == 0:
        print(e)

    # Gradient descent weight udpate with regularization.
    # w += learning_rate * ( np.dot((T - Y).T, Xb) - 0.01*w ) # old
    w += learning_rate * ( Xb.T.dot(T - Y) - 0.1*w )

    # Recalculate Y.
    Y = sigmoid(Xb.dot(w))

plt.plot(error)
plt.title("Cross-entropy per iteration")
plt.show()

print("Final w:", w)
print("Final classification rate:", 1 - np.abs(T - np.round(Y)).sum() / N)