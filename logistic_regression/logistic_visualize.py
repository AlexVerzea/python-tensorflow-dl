# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import numpy as np
import matplotlib.pyplot as plt
from builtins import range

N = 100
D = 2

X = np.random.randn(N,D)

# Center the first 50 points at (-2,-2).
X[:50,:] = X[:50,:] - 2 * np.ones((50,D))

# Center the last 50 points at (2, 2).
X[50:,:] = X[50:,:] + 2 * np.ones((50,D))

# Labels: first 50 are 0, last 50 are 1.
T = np.array([0]*50 + [1]*50)

# Add a column of ones.
# ones = np.array([[1]*N]).T
ones = np.ones((N, 1))
Xb = np.concatenate((ones, X), axis=1)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

# Get the closed-form solution.
w = np.array([0, 4, 4])

# calculate the model output
z = Xb.dot(w)
Y = sigmoid(z)

plt.scatter(X[:,0], X[:,1], c=T, s=100, alpha=0.5)

x_axis = np.linspace(-6, 6, 100)
y_axis = -x_axis
plt.plot(x_axis, y_axis)
plt.show()
