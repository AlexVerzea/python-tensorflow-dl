# N.B. Uses mushroom dataset from: https://archive.ics.uci.edu/ml/datasets/Mushroom
# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from future.utils import iteritems
from builtins import range, input
from sklearn.model_selection import cross_val_score

NUMERICAL_COLS = ()
CATEGORICAL_COLS = np.arange(22) + 1 # 1..22 inclusive

# N.B. Transforms data from dataframe to numerical matrix.
# N.B. One-hot encodes categories and normalizes numerical columns.
# N.B. We want to use the scales found in training when transforming the test set so we only call fit() once.
# N.B. Call transform() for any subsequent data.
class DataTransformer:
  def fit(self, df):
    self.labelEncoders = {}
    self.scalers = {}
    for col in NUMERICAL_COLS:
      scaler = StandardScaler()
      scaler.fit(df[col].reshape(-1, 1))
      self.scalers[col] = scaler

    for col in CATEGORICAL_COLS:
      encoder = LabelEncoder()
      # In case the train set does not have 'missing' value but the test set does.
      values = df[col].tolist()
      values.append('missing')
      encoder.fit(values)
      self.labelEncoders[col] = encoder

    # Find dimensionality.
    self.D = len(NUMERICAL_COLS)
    for col, encoder in iteritems(self.labelEncoders):
      self.D += len(encoder.classes_)
    print("dimensionality:", self.D)

  def transform(self, df):
    N, _ = df.shape
    X = np.zeros((N, self.D))
    i = 0
    for col, scaler in iteritems(self.scalers):
      X[:,i] = scaler.transform(df[col].values.reshape(-1, 1)).flatten()
      i += 1

    for col, encoder in iteritems(self.labelEncoders):
      # print "transforming col:", col
      K = len(encoder.classes_)
      X[np.arange(N), encoder.transform(df[col]) + i] = 1
      i += K
    return X

  def fit_transform(self, df):
    self.fit(df)
    return self.transform(df)


def replace_missing(df):
  # Standard method of replacement for numerical columns is median.
  for col in NUMERICAL_COLS:
    if np.any(df[col].isnull()):
      med = np.median(df[ col ][ df[col].notnull() ])
      df.loc[ df[col].isnull(), col ] = med

  # Set a special value = 'missing'.
  for col in CATEGORICAL_COLS:
    if np.any(df[col].isnull()):
      print(col)
      df.loc[ df[col].isnull(), col ] = 'missing'

def get_data():
  df = pd.read_csv('../large_files/mushroom.data', header=None)

  # Replace label column: e/p --> 0/1
  # e = edible = 0, p = poisonous = 1
  df[0] = df.apply(lambda row: 0 if row[0] == 'e' else 1, axis=1)

  # Check if there is missing data.
  replace_missing(df)

  # Transform the data
  transformer = DataTransformer()

  X = transformer.fit_transform(df)
  Y = df[0].values
  return X, Y

if __name__ == '__main__':
  X, Y = get_data()

  # Do a quick baseline test.
  baseline = LogisticRegression()
  print("CV baseline:", cross_val_score(baseline, X, Y, cv=8).mean())

  # Single tree.
  tree = DecisionTreeClassifier()
  print("CV one tree:", cross_val_score(tree, X, Y, cv=8).mean())

  model = RandomForestClassifier(n_estimators=20) # try 10, 20, 50, 100, 200
  print("CV forest:", cross_val_score(model, X, Y, cv=8).mean())