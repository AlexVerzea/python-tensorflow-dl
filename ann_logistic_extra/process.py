# N.B. May need to update the version of future: sudo pip install -U 

# Imports
# from __future__ MUST be at the beginning!
from __future__ import print_function, division
import numpy as np
import pandas as pd
import os
from builtins import range

# Scripts from other folders can now import this file.
dir_path = os.path.abspath(os.path.dirname(os.path.realpath('ecommerce_data.csv')))

# Method 1.
# Normalize numerical columns.
# One-hot categorical columns.
def get_data():
  df = pd.read_csv(dir_path + '/ecommerce_data.csv')

  # Check headers.
  # df.head()

  # Easier to work with numpy array.
  data = df.values

  # Shuffle it.
  np.random.shuffle(data)

  # Split features and labels.
  X = data[:,:-1]
  Y = data[:,-1].astype(np.int32)

  # One-hot encode the categorical data.
  # Create a new matrix X2 with the correct number of columns.
  N, D = X.shape
  X2 = np.zeros((N, D + 3))
  X2[:,0:(D - 1)] = X[:,0:(D - 1)] # Non-Categorical

  # One-hot
  for n in range(N):
      t = int(X[n,D - 1])
      X2[n, t + D - 1] = 1

  # Method 2.
  # Z = np.zeros((N, 4))
  # Z[np.arange(N), X[:,D-1].astype(np.int32)] = 1
  # # assign: X2[:,-4:] = Z
  # assert(np.abs(X2[:,-4:] - Z).sum() < 1e-10)

  # Assign X2 back to X, since we don't need original anymore.
  X = X2

  # Split train and test.
  Xtrain = X[:-100]
  Ytrain = Y[:-100]
  Xtest = X[-100:]
  Ytest = Y[-100:]

  # Normalize columns 1 and 2.
  for i in (1, 2):
    m = Xtrain[:,i].mean()
    s = Xtrain[:,i].std()
    Xtrain[:,i] = (Xtrain[:,i] - m) / s
    Xtest[:,i] = (Xtest[:,i] - m) / s

  return Xtrain, Ytrain, Xtest, Ytest


def get_binary_data():
  # Return only the data from the first 2 classes.
  Xtrain, Ytrain, Xtest, Ytest = get_data()
  X2train = Xtrain[Ytrain <= 1]
  Y2train = Ytrain[Ytrain <= 1]
  X2test = Xtest[Ytest <= 1]
  Y2test = Ytest[Ytest <= 1]
  return X2train, Y2train, X2test, Y2test