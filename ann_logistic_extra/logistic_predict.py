# N.B. May need to update the version of future: sudo pip install -U 

# Imports
# from __future__ MUST be at the beginning!
from __future__ import print_function, division
import numpy as np
from builtins import range
from process import get_binary_data

X, Y, _, _ = get_binary_data()

# Randomly initialize weights.
D = X.shape[1]
W = np.random.randn(D)
b = 0 # Bias term.

# Make predictions.
def sigmoid(a):
    return 1 / (1 + np.exp(-a))

def forward(X, W, b):
    return sigmoid(X.dot(W) + b)

P_Y_given_X = forward(X, W, b)
predictions = np.round(P_Y_given_X)

# Calculate the accuracy.
def classification_rate(Y, P):
    return np.mean(Y == P)

print("Score:", classification_rate(Y, predictions))