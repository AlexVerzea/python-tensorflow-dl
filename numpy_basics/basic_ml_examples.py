# Airfoil data:
# https://archive.ics.uci.edu/ml/datasets/Airfoil+Self-Noise

# N.B. May need to update the version of future: sudo pip install -U future
# N.B. pandas is moving from .as_matrix() to the equivalent .values

# Imports
import numpy as np
import pandas as pd
from __future__ import print_function, division
from future.utils import iteritems
from builtins import range, input
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler

# Load the data.
df = pd.read_csv('../large_files/airfoil_self_noise.dat', sep='\t', header=None)

# Inspect data headers and info.
df.head()
df.info()

# Retrieve inputs from data.
data = df[[0,1,2,3,4]].values

# Retrieve outputs from data.
target = df[5].values

# Split the data set into Train and Test sets: allows us to us simulate how the model 
# will perform in the future.
X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.33)

# Instantiate a LR model and train it.
model = LinearRegression()
model.fit(X_train, y_train)]

# Evaluate the model's performance (R^2).
print(model.score(X_train, y_train))
print(model.score(X_test, y_test))

# Get predictions on the Test set.
model.predict(X_test)

# Instantiate a RF model and train it.
model2 = RandomForestRegressor()
model2.fit(X_train, y_train)

# Evaluate the model's performance.
print(model2.score(X_train, y_train))
print(model2.score(X_test, y_test))

# Instantiate a Random Forest model and train it. Note the use of scaling.
scaler = StandardScaler()
X_train2 = scaler.fit_transform(X_train)
X_test2 = scaler.transform(X_test)
scaler2 = StandardScaler()
y_train2 = scaler2.fit_transform(np.expand_dims(y_train, -1)).ravel()
y_test2 = scaler2.fit_transform(np.expand_dims(y_test, -1)).ravel()
model = MLPRegressor(max_iter=500)
model.fit(X_train2, y_train2)

# Evaluate the model's performance. Better than LR but worse than RF.
print(model.score(X_train2, y_train2))
print(model.score(X_test2, y_test2))
