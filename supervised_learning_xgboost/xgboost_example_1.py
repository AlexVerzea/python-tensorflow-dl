# Do not call this file xgboost.py as that would overwrite the xgboost module.
# N.B. May need to update the version of future: sudo pip install -U
# Template taken from: https://github.com/yuxiangdai/machine-learning-templates/blob/master/XGBoost/xgboost.py

# Imports
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score

# Importing the dataset.
# We try to predict the customers of the bank that will leave the bank.
# Binary Classifier: Independent variables are all variables except Exited,
# which says whether or not the customer left the bank (1 if true).
dataset = pd.read_csv('../large_files/Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values

# Encoding categorical data.
labelencoder_X_1 = LabelEncoder()
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])
labelencoder_X_2 = LabelEncoder()
X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])
onehotencoder = OneHotEncoder(categorical_features=[1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set.
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=0)

# Fitting XGBoost to the Training set.
classifier = XGBClassifier()
classifier.fit(X_train, y_train)

# Predicting the Test set results.
y_pred = classifier.predict(X_test)

# Applying k-Fold Cross Validation.
accuracies = cross_val_score(estimator=classifier, X=X_train, y=y_train, cv=10)
print("Accuracies Mean:", accuracies.mean())
print("Accuracies Standard Deviation:", accuracies.std())
