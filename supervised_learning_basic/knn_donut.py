# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
from builtins import range, input
from knn import KNN
from util import get_donut
import matplotlib.pyplot as plt

if __name__ == '__main__':
    X, Y = get_donut()

    # Display the data.
    plt.scatter(X[:,0], X[:,1], s=100, c=Y, alpha=0.5)
    plt.show()

    # Get the accuracy.
    model = KNN(3)
    model.fit(X, Y)
    print("Accuracy:", model.score(X, Y))