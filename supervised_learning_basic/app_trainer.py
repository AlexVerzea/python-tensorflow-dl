# N.B. May need to update the version of future: sudo pip install -U 

# Imports
from __future__ import print_function, division
import pickle
import numpy as np
from util import get_data
from sklearn.ensemble import RandomForestClassifier
from builtins import range, input

if __name__ == '__main__':
    X, Y = get_data()
    Ntrain = len(Y) // 4
    Xtrain, Ytrain = X[:Ntrain], Y[:Ntrain]

    model = RandomForestClassifier()
    model.fit(Xtrain, Ytrain)

    # Just in case you're curious.
    Xtest, Ytest = X[Ntrain:], Y[Ntrain:]
    print("test accuracy:", model.score(Xtest, Ytest))

    with open('mymodel.pkl', 'wb') as f:
        pickle.dump(model, f)
